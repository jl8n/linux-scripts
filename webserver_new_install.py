import os
import sys
import distro


def modify_line(path, filename, old_line, new_line):
    """
    Make a copy of a given file
    Find the first line in that file that matches a given string
    Replace the old string with a given new string in the copied file
    Replace the copied file with the original  
    """

    temp_filepath = '/tmp/{}.tmp'.format(filename)
    os.system('sudo cp {}{} {}'.format(path, filename, temp_filepath))
    rootIndex = 0

    if new_line[-1] != '\n':
        new_line += '\n'

    with open(temp_filepath, 'r') as fin:
        lines = fin.readlines()
    for i in range(len(lines)):
        if lines[i].strip() == old_line:
            rootIndex = i
            break
    
    lines[rootIndex] = new_line
    print(lines)
    
    with open(path+filename, 'w') as fout:
        fout.writelines(lines)


def test():
    modify_line(
        '/etc/',
        'passwd',
        'root:x:0:0:root:/root:/bin/bash',
        'root:x:0:0:root:/root:/sbin/nologin'
    )
   

def disable_root_account(username):
    os.system('useradd -m admin')
    os.system('passwd admin')
    os.system('usermod -aG sudo admin')  # give admin root privelage
    os.system('su admin')
    modify_line(
        '/etc/',
        'passwd',
        'root:x:0:0:root:/root:/bin/bash',
        'root:x:0:0:root:/root:/sbin/nologin'
    )


    

    
    

def ufw_config():
    os.system('sudo apt-get install ufw')
    os.system('sudo ufw enable')
    os.system('sudo ufw allow 22')  # ssh
    os.system('sudo ufw allow 80')  # http
    os.system('sudo ufw allow 443')  # https

def debian_config():
    os.system('sudo apt-get install nginx git ufw')

    # enable shell colors
    with open('~/.bashrc', 'a+') as fout:
        fout.write("""
            export LS_OPTIONS='--color=auto'
            eval "`dircolors`"
            alias ls='ls $LS_OPTIONS'
        """)   


def ubuntu_config():
    print('ubuntu detected')


def main():
    if not os.geteuid() == 0:
        sys.exit('This script must be run as root!')

    d = distro.linux_distribution(full_distribution_name=False)
    d = d[0]
    test()

    if d == 'debian':
        print('running debian config...')
        #debian_config()
    elif d == 'ubuntu':
        ubuntu_config()


if __name__ == '__main__':
    main()